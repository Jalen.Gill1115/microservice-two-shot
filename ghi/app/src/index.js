import React from 'react';
import ReactDOM from 'react-dom/client';
import App from './App';

const root = ReactDOM.createRoot(document.getElementById('root'));


async function getHatsandShoes() {
  const hatResponse = await fetch('http://localhost:8090/api/hats');
  const shoeResponse =await fetch('http://localhost:8080/api/shoes');
  console.log(hatResponse);
  console.log(shoeResponse);
  const hatData = await hatResponse.json();
  const shoeData = await shoeResponse.json();
  console.log(hatData);
  console.log(shoeData);
  if (hatResponse.ok && shoeResponse.ok) {
    root.render(
      <React.StrictMode>
        <App hats={hatData.hats} shoes={shoeData.shoes}/>
      </React.StrictMode>
    )
  }
  else {
    console.error("Didn't get a response")
  }

}
getHatsandShoes();
