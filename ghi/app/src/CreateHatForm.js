import React, { useEffect, useState } from 'react';

function CreateHatForm() {

    useEffect(() => {
        const getLocations = async () => {
        const url = 'http://localhost:8100/api/locations/';

        const response = await fetch(url);
        console.log(response)
        if (response.ok) {
            const data = await response.json();
            setLocations(data.locations)
        }

        }
        getLocations();

    }, [])

    const [styleName, setStyleName] = useState('');
    const [color, setColor] = useState('');
    const [fabric, setFabric] = useState('');
    const [pictureUrl, setpictureUrl] = useState('');
    const [location, setLocation] = useState('');
    const [locations, setLocations] = useState([]);

    async function handleSubmit(event) {
        event.preventDefault();
        const data = {
            fabric: fabric,
            style_name: styleName,
            color: color,
            picture_url: pictureUrl,
            location: parseInt(location)
        };
        console.log(data)

        const hatUrl = 'http://localhost:8090/api/hats/'
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
              'Content-Type': 'application/json',
            },
        };
        const response = await fetch(hatUrl, fetchConfig);

        if (response.ok) {
            const postdata = await response.json();
            console.log(postdata)

            alert("Hat created successfully");
            for (const key in data) {
                data[key] = ''
            }
        }
    }
        return (
            <div className="container">
                <div className="row">
                <div className="offset-3 col-6">
                    <div className="shadow p-3 mt-5">
                        <h1 className="offset-3 col-6">Add a new hat</h1>
                        <form onSubmit={handleSubmit} id="create-conference-form">
                        <div className="form-floating mb-3">
                            <input value={styleName} onChange={(event) => setStyleName(event.target.value)} placeholder="Name" required type="text" name="name" id="name" className="form-control"/>
                            <label htmlFor="name">Style name</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input value={fabric} onChange={(event) => setFabric(event.target.value)} placeholder="Starts" required type="text" name="starts" id="starts" className="form-control"/>
                            <label htmlFor="Fabric">Fabric</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input value={color}  onChange={(event) => setColor(event.target.value)} placeholder="Ends" required type="text" name="ends" id="ends" className="form-control"/>
                            <label htmlFor="Color">Color</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input value={pictureUrl}  onChange={(event) => setpictureUrl(event.target.value)} placeholder="Picture URL" required type="text" name="description" id="Description" className="form-control" rows="3"/>
                            <label htmlFor="room_count">Picture Url</label>
                        </div>
                        <div className="mb-3">
                            <select value={location}  onChange={(event) => setLocation(event.target.value)} required id="location" name="location" className="form-select">
                            <option value="">Choose a location</option>
                            {locations?.map(location => {
                                            return (
                                            <option key={location.id} value={location.id}>
                                                {location.closet_name}
                                            </option>
                                            )
                                            })}

                            </select>
                        </div>
                        <button className="btn btn-primary">Create</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        )
}


export default CreateHatForm;
