import React, { useState } from 'react';
import { NavLink } from 'react-router-dom';

function HatList(props) {

    const [hats, updateHats] = useState([...props.hats]);
    console.log(hats);

    return (
        <>
        <div className="text-center">
            <h1>Your hats</h1>
            <NavLink type="button" className="btn btn-success" to="/hats/create">Add a new hat</NavLink>
        </div>

        <table className="table">
            <thead>
                <tr>
                    <th>Name</th>
                    <th>Fabric</th>
                    <th>Color</th>
                    <th>Location</th>
                </tr>
            </thead>
            <tbody>
            { hats?.map( hat => {
                return (
                    <tr key={hat.id}>
                        <td>{hat.style_name}</td>
                        <td>{hat.fabric}</td>
                        <td>{hat.color}</td>
                        <td>
                            <div>{hat.location.closet_name}</div>
                            <div>Section #: {hat.location.section_number}</div>
                            <div>Shelf #: {hat.location.shelf_number}</div>
                        </td>
                        <td><img src={hat.picture_url} style={{height: "100px"}}></img></td>
                        <td><button type="button" className="btn btn-danger"
                            onClick={async () => {
                                updateHats(
                                    hats.filter(h => h.id !== hat.id),
                                    await fetch(`http://localhost:8090/api/hats/${hat.id}`, {method: 'DELETE'}),
                                )
                                }
                            }>Delete</button></td>
                    </tr>

                )

                })}
                </tbody>
        </table>
        </>
        )
}

export default HatList;
