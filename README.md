# Wardrobify

Team:

* Jalen Gill - Hats Microservice
* Kyle - Shoes Microservice

## Design

## Shoes microservice

Explain your models and integration with the wardrobe
microservice, here.

## Hats microservice

For my microservice, I created two Models that would be used to store date, these being Hat and a LocationVO which would contain location data from the wardrobe microservice. The Hat model contained several fields that described its name, fabric, color, and a URL to show a picture. For the LocationVO, I created a poller that would check for any updates in the Location model and update or create a new LocationVO depending on the changes made to the Location model.

To convert my data into JSON, I created 2 view functions that allowed me to see all of the hats created, to create a new hat, and to delete an existing hat. This was done using RESTful APIs in order to send different request methodds to the same URL to perform different actions respectively.

Once the data was converted into JSON, I moved to the React App where I created a list page to show each hat, along with a delete button for each hat. I also created a create hat page that allows you to put in all the attributes of the hat, and make a post request to the backend to create a new hat.
