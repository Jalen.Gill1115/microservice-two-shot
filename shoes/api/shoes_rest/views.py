import json
from django.http import JsonResponse
from django.shortcuts import render
from common.json import ModelEncoder

from .models import BinVO, Shoe
from django.views.decorators.http import require_http_methods

class BinVOEncoder(ModelEncoder):
    model = BinVO
    properties = [
        "closet_name",
        "bin_number",
        "bin_size",
    ]
class ShoeDetailEncoder(ModelEncoder):
    model = Shoe
    properties = [
        "manufacturer",
        "model_name",
        "color",
        "picture",
        "bin",
    ]
    encoders = {
        "bin": BinVOEncoder(),
    }    

class ShoeListEncoder(ModelEncoder):
    model = Shoe
    properties = [
        "manufacturer",
        "model_name",
        "color",
        "picture",
        "bin",
        ]
    encoders = {
        "bin": BinVOEncoder()
    }

@require_http_methods(["GET", "POST"])
def api_list_shoes(request):
    if request.method == "GET":
        shoes = Shoe.objects.all()
        return JsonResponse(
            {"shoes": shoes},
            encoder=ShoeListEncoder
        )
    else:
        content = json.loads(request.body)
        try:
            bin = BinVO.objects.get(id=content['bin'])
            content["bin"] = bin
        except BinVO.DoesNotExist:
            return JsonResponse(
                    {"message": "Invalid location id"},
                    status=400,
                )
        shoe = Shoe.objects.create(**content)
        return JsonResponse(
            shoe,
            encoder=ShoeDetailEncoder,
            safe=False 
        )
        
@require_http_methods(["GET", "DELETE"])
def api_shoe_detail(request, id):
    if request.method == "GET":
        try:
            shoe = Shoe.objects.all(id=id)
        except Shoe.DoesNotExist:
            return JsonResponse(
                {"message": "console: "},
                status=400,
            )
        return JsonResponse(
            Shoe,
            encoder=ShoeDetailEncoder,
            safe=False,
        )
    else:
        Shoe.objects.get(id=id).delete()
        return JsonResponse(
            {"message": "console: "},
            status=200
        )
                

