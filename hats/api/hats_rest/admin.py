from django.contrib import admin
from .models import LocationVO
# Register your models here.
@admin.register(LocationVO)
class LocationVOAdmin(admin.ModelAdmin):
    list_display = (
        'closet_name',
        'location_href',
        )
