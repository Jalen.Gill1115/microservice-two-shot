from django.db import models


class LocationVO(models.Model):
    closet_name = models.CharField(max_length=100)
    section_number = models.PositiveSmallIntegerField(null=True)
    shelf_number = models.PositiveSmallIntegerField(null=True)
    location_href = models.CharField(max_length=300, unique=True, null=True)

    def __str__(self):
        return self.closet_name


class Hat(models.Model):
    style_name = models.CharField(max_length=30)
    fabric = models.CharField(max_length=30)
    color = models.CharField(max_length=30)
    picture_url = models.URLField(null=True)
    location = models.ForeignKey(
        'LocationVO',
        related_name='hats',
        on_delete=models.CASCADE,
    )

    def __str__(self):
        return self.fabric
